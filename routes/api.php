<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Enterprise')->group(function () {
	Route::resource('enterprises', 'DefaultController', ['except' => ['create', 'edit']]);
	Route::group(['middleware' => ['auth:api'], 'prefix' => 'enterprises/{enterprise_id}'], function () {
		Route::resource('addresses', 'AddressController', ['except' => ['create', 'edit']]);
		Route::resource('contacts', 'ContactController', ['except' => ['create', 'edit']]);
		Route::resource('drivers', 'DriverController', ['except' => ['create', 'edit']]);
		Route::resource('vehicles', 'VehicleController', ['except' => ['create', 'edit']]);
		Route::resource('routePlannings', 'RoutePlanningController', ['except' => ['create', 'edit']]);
		Route::group(['prefix' => 'routePlannings/{enterprise_route_planning_id}'], function () {
			Route::resource('routePlanningWaypoints', 'RoutePlanningWaypointController', ['except' => ['create', 'edit']]);
		});
	});
});

Route::group(['namespace' => 'User', 'middleware' => ['auth:api'], 'prefix' => 'enterprises/{enterprise_id}'], function () {
	Route::resource('users', 'DefaultController', ['except' => ['create', 'edit']]);
});
