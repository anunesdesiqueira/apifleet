<?php

namespace App\Repositories\User;

interface DefaultRepository 
{
	function all($enterprise_id);

	function show($enterprise_id, $id);

	function store($enterprise_id, array $attributes);

	function update($enterprise_id, array $attributes, $id);

	function destroy($enterprise_id, $id);
}
