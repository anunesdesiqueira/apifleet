<?php

namespace App\Repositories\User;

use App\Entities\User;

class EloquentDefault implements DefaultRepository
{
	/**
     * @var User
     */
    protected $entity;

    /**
     * EloquentDefault constructor.
     * @param User $entity
     */
    public function __construct(User $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($enterprise_id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->all();
    }

    /**
     * Get by id.
     *
     * @param integer $id
     * @return App\Entities\User
     */
    public function show($id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->firstOrFail();
    }

    /**
     * Create new.
     *
     * @param array $attributes
     * @return App\Entities\User
     */
    public function store(array $attributes)
    {
    	return $this->entity->create($attributes);
    }

    /**
     * Update by id.
     *
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\User
     */
    public function update(array $attributes, $id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->delete();
    }

}
