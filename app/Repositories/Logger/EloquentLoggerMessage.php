<?php

namespace App\Repositories\Logger;

use App\Entities\LoggerMessage;

class EloquentLoggerMessage implements LoggerMessageRepository
{
	/**
     * @var LoggerMessage
     */
    protected $entity;

    /**
     * EloquentLoggerMessage constructor.
     * @param LoggerMessage $entity
     */
    public function __construct(LoggerMessage $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
    	return $this->entity->all();
    }

    /**
     * Get by id.
     *
     * @param integer $id
     * @return App\Entities\LoggerMessage
     */
    public function show($id)
    {
    	return $this->entity->findOrFail($id);
    }

    /**
     * Create new.
     *
     * @param array $attributes
     * @return App\Entities\LoggerMessage
     */
    public function store(array $attributes)
    {
        return $this->entity->create($attributes);
    }

    /**
     * Update by id.
     *
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\LoggerMessage
     */
    public function update(array $attributes, $id)
    {
    	return $this->entity->find($id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity->find($id)->delete();
    }

}
