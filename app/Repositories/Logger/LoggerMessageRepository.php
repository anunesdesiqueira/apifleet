<?php

namespace App\Repositories\Logger;

interface LoggerMessageRepository 
{
	function all();

	function show($id);

	function store(array $attributes);

	function update(array $attributes, $id);

	function destroy($id);
}
