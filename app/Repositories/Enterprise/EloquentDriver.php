<?php

namespace App\Repositories\Enterprise;

use App\Entities\EnterpriseDriver;

class EloquentDriver implements DriverRepository
{
	/**
     * @var EnterpriseDriver
     */
    protected $entity;

    /**
     * EloquentDriver constructor.
     * @param EnterpriseDriver $entity
     */
    public function __construct(EnterpriseDriver $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @param integer $enterprise_id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($enterprise_id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->all();
    }

    /**
     * Get by id.
     *
     * @param integer $enterprise_id
     * @param integer $id
     * @return App\Entities\EnterpriseDriver
     */
    public function show($enterprise_id, $id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->firstOrFail();
    }

    /**
     * Create new.
     *
     * @param integer $enterprise_id
     * @param array $attributes
     * @return App\Entities\EnterpriseDriver
     */
    public function store($enterprise_id, array $attributes)
    {
        return $this->entity->create($attributes);
    }

    /**
     * Update by id.
     *
     * @param integer $enterprise_id
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\EnterpriseDriver
     */
    public function update($enterprise_id, array $attributes, $id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->delete();
    }

}
