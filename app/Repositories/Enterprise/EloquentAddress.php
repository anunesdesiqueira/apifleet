<?php

namespace App\Repositories\Enterprise;

use App\Entities\EnterpriseAddress;

class EloquentAddress implements AddressRepository
{
	/**
     * @var EnterpriseAddress
     */
    protected $entity;

    /**
     * EloquentAddress constructor.
     * @param EnterpriseAddress $entity
     */
    public function __construct(EnterpriseAddress $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @param integer $enterprise_id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($enterprise_id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->all();
    }

    /**
     * Get by id.
     *
     * @param integer $enterprise_id
     * @param integer $id
     * @return App\Entities\EnterpriseAddress
     */
    public function show($enterprise_id, $id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->firstOrFail();
    }

    /**
     * Create new.
     *
     * @param integer $enterprise_id
     * @param array $attributes
     * @return App\Entities\EnterpriseAddress
     */
    public function store($enterprise_id, array $attributes)
    {
        return $this->entity->create($attributes);
    }

    /**
     * Update by id.
     *
     * @param integer $enterprise_id
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\EnterpriseAddress
     */
    public function update($enterprise_id, array $attributes, $id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity
            ->findEnterprise($enterprise_id)->where('id', $id)->delete();
    }

}
