<?php

namespace App\Repositories\Enterprise;

use App\Entities\EnterpriseRoutePlanningWaypoint;

class EloquentRoutePlanningWaypoint implements RoutePlanningWaypointRepository
{
	/**
     * @var EnterpriseRoutePlanningWaypoint
     */
    protected $entity;

    /**
     * EloquentRoutePlanningWaypoint constructor.
     * @param EnterpriseRoutePlanningWaypoint $entity
     */
    public function __construct(EnterpriseRoutePlanningWaypoint $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @param integer $enterprise_route_planning_id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($enterprise_route_planning_id)
    {
    	return $this->entity
            ->where('enterprise_route_planning_id', $enterprise_route_planning_id)->all();
    }

    /**
     * Get by id.
     *
     * @param integer $enterprise_route_planning_id
     * @param integer $id
     * @return App\Entities\EnterpriseRoutePlanningWaypoint
     */
    public function show($enterprise_route_planning_id, $id)
    {
    	return $this->entity
            ->where('enterprise_route_planning_id', $enterprise_route_planning_id)->where('id', $id)->firstOrFail();
    }

    /**
     * Create new.
     *
     * @param integer $enterprise_route_planning_id
     * @param array $attributes
     * @return App\Entities\EnterpriseRoutePlanningWaypoint
     */
    public function store($enterprise_route_planning_id, array $attributes)
    {
        return $this->entity->create($attributes);
    }

    /**
     * Update by id.
     *
     * @param integer $enterprise_route_planning_id
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\EnterpriseRoutePlanningWaypoint
     */
    public function update($enterprise_route_planning_id, array $attributes, $id)
    {
    	return $this->entity
            ->where('enterprise_route_planning_id', $enterprise_route_planning_id)->where('id', $id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity
            ->where('enterprise_route_planning_id', $enterprise_route_planning_id)->where('id', $id)->delete();
    }

}
