<?php

namespace App\Repositories\Enterprise;

interface ContactRepository 
{
	function all($enterprise_id);

	function show($enterprise_id, $id);

	function store($enterprise_id, array $attributes);

	function update($enterprise_id, array $attributes, $id);

	function destroy($enterprise_id, $id);
}
