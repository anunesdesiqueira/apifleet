<?php

namespace App\Repositories\Enterprise;

use App\Entities\Enterprise;

class EloquentDefault implements DefaultRepository
{
	/**
     * @var Enterprise
     */
    protected $entity;

    /**
     * EloquentDefault constructor.
     * @param Enterprise $entity
     */
    public function __construct(Enterprise $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get all.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
    	return $this->entity->all();
    }

    /**
     * Get by id.
     *
     * @param integer $id
     * @return App\Entities\Enterprise
     */
    public function show($id)
    {
    	return $this->entity->findOrFail($id);
    }

    /**
     * Create new.
     *
     * @param array $attributes
     * @return App\Entities\Enterprise
     */
    public function store(array $attributes)
    {
        $enterprise = $this->entity->create($attributes);
        $user = $enterprise->users()->create($attributes['user']);
        $user->activation()->create(['token' => env('APP_KEY') . $user->id . $user->email]);

        return $enterprise;
    }

    /**
     * Update by id.
     *
     * @param array $attributes
     * @param integer $id
     * @return App\Entities\Enterprise
     */
    public function update(array $attributes, $id)
    {
    	return $this->entity->find($id)->update($attributes);
    }

    /**
     * Destroy by id.
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
    	return $this->entity->find($id)->delete();
    }

    /**
     * Enterprise get last user insert by id.
     *
     * @param integer $id
     * @return App\Entities\Enterprise
     */
    public function enterpriseGetLastUserInsert($id)
    {
        return $this->entity->with([
                'users' => function($query) {
                    $query->orderBy('id', 'desc')->limit(1);
                },
                'users.status',
                'users.activation',
            ])->find($id);
    }

}
