<?php

namespace App\Repositories\Enterprise;

interface DefaultRepository 
{
	function all();

	function show($id);

	function store(array $attributes);

	function update(array $attributes, $id);

	function destroy($id);

	function enterpriseGetLastUserInsert($id);
}
