<?php

namespace App\Repositories\Enterprise;

interface RoutePlanningWaypointRepository 
{
	function all($enterprise_route_planning_id);

	function show($enterprise_route_planning_id, $id);

	function store($enterprise_route_planning_id, array $attributes);

	function update($enterprise_route_planning_id, array $attributes, $id);

	function destroy($enterprise_route_planning_id, $id);
}
