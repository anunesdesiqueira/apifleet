<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Enterprise\AddressRepository;
use App\Repositories\Enterprise\EloquentAddress;
use App\Repositories\Enterprise\ContactRepository;
use App\Repositories\Enterprise\EloquentContact;
use App\Repositories\Enterprise\DriverRepository;
use App\Repositories\Enterprise\EloquentDriver;
use App\Repositories\Enterprise\DefaultRepository as EnterpriseRepository;
use App\Repositories\Enterprise\EloquentDefault as EloquentEnterprise;
use App\Repositories\Enterprise\RoutePlanningRepository;
use App\Repositories\Enterprise\EloquentRoutePlanning;
use App\Repositories\Enterprise\RoutePlanningWaypointRepository;
use App\Repositories\Enterprise\EloquentRoutePlanningWaypoint;
use App\Repositories\Enterprise\VehicleRepository;
use App\Repositories\Enterprise\EloquentVehicle;
use App\Repositories\Logger\LoggerMessageRepository;
use App\Repositories\Logger\EloquentLoggerMessage;
use App\Repositories\User\DefaultRepository as UserRepository;
use App\Repositories\User\EloquentDefault as EloquentUser;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AddressRepository::class, EloquentAddress::class);
        $this->app->singleton(ContactRepository::class, EloquentContact::class);
        $this->app->singleton(DriverRepository::class, EloquentDriver::class);
        $this->app->singleton(EnterpriseRepository::class, EloquentEnterprise::class);
        $this->app->singleton(RoutePlanningRepository::class, EloquentRoutePlanning::class);
        $this->app->singleton(RoutePlanningWaypointRepository::class, EloquentRoutePlanningWaypoint::class);
        $this->app->singleton(VehicleRepository::class, EloquentVehicle::class);
        $this->app->singleton(LoggerMessageRepository::class, EloquentLoggerMessage::class);
        $this->app->singleton(UserRepository::class, EloquentUser::class);
    }
}
