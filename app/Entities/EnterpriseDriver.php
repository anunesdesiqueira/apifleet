<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Illuminate\Database\Eloquent\Model;

class EnterpriseDriver extends Model
{
    use EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'first_name',
        'last_name',
        'birth',
        'license_number',
        'license_type',
        'license_expire_date',
        'license_pic',
    ];

    /**
     * Get the route plannings for the drive.
     */
    public function plannings()
    {
        return $this->hasMany('App\Entities\EnterpriseRoutePlanning');
    }
}
