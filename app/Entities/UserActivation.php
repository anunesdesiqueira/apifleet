<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'token',
        'activated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'activated_at',
    ];

    /**
     * Set bcrypt token for user activation.
     *
     * @param  string  $value
     * @return void
     */
    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = bcrypt($value);
    }

    /**
     * Get the user record associated with the user activation.
     */
    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }
}
