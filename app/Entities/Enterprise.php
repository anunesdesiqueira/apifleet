<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'enterprise_name',
    	'tax_id',
    ];

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['users'];

    /**
     * Get the users for the enterprise.
     */
    public function users()
    {
        return $this->hasMany('App\Entities\User');
    }

    /**
     * Get the addresses for the enterprise.
     */
    public function addresses()
    {
        return $this->hasMany('App\Entities\EnterpriseAddress');
    }

    /**
     * Get the contacts for the enterprise.
     */
    public function contacts()
    {
        return $this->hasMany('App\Entities\EnterpriseContact');
    }

    /**
     * Get the drives for the enterprise.
     */
    public function drives()
    {
        return $this->hasMany('App\Entities\EnterpriseDriver');
    }

    /**
     * Get the plannings for the enterprise.
     */
    public function plannings()
    {
        return $this->hasMany('App\Entities\EnterpriseRoutePlanning');
    }
}
