<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Illuminate\Database\Eloquent\Model;

class EnterpriseContact extends Model
{
    use EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'role',
        'first_name',
        'last_name',
        'landline',
        'mobile_phone',
        'email',
    ];
}
