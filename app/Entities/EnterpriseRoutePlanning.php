<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Illuminate\Database\Eloquent\Model;

class EnterpriseRoutePlanning extends Model
{
    use EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'enterprise_driver_id',
        'enterprise_vehicle_id',
        'status',
        'date',
        'json',
    ];

    /**
     * Get the drive that owns the route planning.
     */
    public function driver()
    {
        return $this->belongsTo('App\Entities\EnterpriseDriver');
    }

    /**
     * Get the vehicle that owns the route planning.
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Entities\EnterpriseVehicle');
    }

    /**
     * Get the waypoints for the route planning.
     */
    public function waypoints()
    {
        return $this->hasMany('App\Entities\EnterpriseRoutePlanningWaypoint');
    }
}
