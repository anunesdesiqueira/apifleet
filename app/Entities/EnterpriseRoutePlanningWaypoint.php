<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EnterpriseRoutePlanningWaypoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_route_planning_id',
        'status',
        'latitude',
        'longitude',
    ];

    /**
     * Get the drive that owns the route planning.
     */
    public function planning()
    {
        return $this->belongsTo('App\Entities\EnterpriseRoutePlanning');
    }
}
