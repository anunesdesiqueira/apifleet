<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'default_language', 
        'email', 
        'password',
        'first_name',
        'last_name',
        'mobile_phone',
        'origin',
        'status_id',
        'unit_of_measure',
        'receive_alert_by_email',
        'picture',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
    ];

    /**
     * Set bcrypt password for user.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Set ucfirst first_name for user.
     *
     * @param  string  $value
     * @return void
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst(strtolower($value));
    }

    /**
     * Set ucfirst last_name for user.
     *
     * @param  string  $value
     * @return void
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucfirst(strtolower($value));
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    /**
     * Get the status record associated with the user.
     */
    public function status()
    {
        return $this->belongsTo('App\Entities\UserStatus');
    }

    /**
     * Get the activation record associated with the user.
     */
    public function activation()
    {
        return $this->hasOne('App\Entities\UserActivation');
    }
}
