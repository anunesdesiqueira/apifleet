<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Illuminate\Database\Eloquent\Model;

class EnterpriseVehicle extends Model
{
    use EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'number_plate',
        'color',
        'type',
        'load',
    ];

    /**
     * Get the route plannings for the drive.
     */
    public function plannings()
    {
        return $this->hasMany('App\Entities\EnterpriseRoutePlanning');
    }
}
