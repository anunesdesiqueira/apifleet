<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LoggerMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'type',
    	'service',
    	'identifier',
    	'recipient',
    	'content',
    ];

    /**
     * Get the user record associated with the logger message.
     */
    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }
}
