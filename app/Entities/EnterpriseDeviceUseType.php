<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EnterpriseDeviceUseType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'device_use_type',
    ];
}
