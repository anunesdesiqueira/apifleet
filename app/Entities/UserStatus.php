<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'status_name',
    ];

    /**
     * Get the users for the drive.
     */
    public function users()
    {
        return $this->hasMany('App\Entities\User');
    }
}
