<?php

namespace App\Entities;

use App\Traits\EnterpriseTrait;
use Illuminate\Database\Eloquent\Model;

class EnterpriseAddress extends Model
{
    use EnterpriseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id',
        'type',
        'address',
        'data_address',
    ];
}
