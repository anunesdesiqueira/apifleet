<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreDefault;
use App\Http\Requests\User\UpdateDefault;
use App\Services\User\DefaultService as UserService;

class DefaultController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * DefaultController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int  $enterprise_id
     * @param  StoreDefault  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreDefault $request)
    {
        $data = $request->only([
            'enterprise_id',
            'default_language', 
            'email',
            'password',
            'first_name',
            'last_name',
            'mobile_phone',
            'origin',
            'status_id',
            'unit_of_measure',
            'picture',
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int  $enterprise_id
     * @param  UpdateDefault  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateDefault $request, $id)
    {
        $data = $request->only([
            'id',
            'default_language', 
            'email', 
            'password',
            'first_name',
            'last_name',
            'mobile_phone',
            'origin',
            'status_id',
            'unit_of_measure',
            'receive_alert_by_email',
            'picture',
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
