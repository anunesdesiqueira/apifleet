<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreContact;
use App\Http\Requests\Enterprise\UpdateContact;
use App\Services\Enterprise\ContactService;

class ContactController extends Controller
{
    /**
     * @var ContactService
     */
    protected $service;

    /**
     * ContactController constructor.
     * @param ContactService $service
     */
    public function __construct(ContactService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  StoreContact  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreContact $request)
    {
        $data = $request->only([
            'enterprise_id',
	        'role',
	        'first_name',
	        'last_name',
	        'landline',
	        'mobile_phone',
	        'email',
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  UpdateContact  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateContact $request, $id)
    {
        $data = $request->only([
            'id',
            'role',
	        'first_name',
	        'last_name',
	        'landline',
	        'mobile_phone',
	        'email',
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
