<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreVehicle;
use App\Http\Requests\Enterprise\UpdateVehicle;
use App\Services\Enterprise\VehicleService;

class VehicleController extends Controller
{
    /**
     * @var VehicleService
     */
    protected $service;

    /**
     * VehicleController constructor.
     * @param VehicleService $service
     */
    public function __construct(VehicleService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  StoreVehicle  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreVehicle $request)
    {
        $data = $request->only([
            'enterprise_id',
	        'number_plate',
	        'color',
	        'type',
	        'load',
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  UpdateVehicle  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateVehicle $request, $id)
    {
        $data = $request->only([
            'id',
	        'number_plate',
	        'color',
	        'type',
	        'load',
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
