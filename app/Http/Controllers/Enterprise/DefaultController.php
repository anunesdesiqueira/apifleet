<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreDefault;
use App\Http\Requests\Enterprise\UpdateDefault;
use App\Services\Enterprise\DefaultService as EnterpriseService;

class DefaultController extends Controller
{
    /**
     * @var EnterpriseService
     */
    protected $service;

    /**
     * DefaultController constructor.
     * @param EnterpriseService $service
     */
    public function __construct(EnterpriseService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->service->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDefault  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDefault $request)
    {
        $data = $request->only([
            'enterprise_name', 
            'tax_id', 
            'user',
        ]);
        
        return $this->service->store($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateDefault  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDefault $request, $id)
    {
        $data = $request->only([
            'id', 
            'enterprise_name', 
            'tax_id',
        ]);

        return $this->service->update($data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->service->destroy($id);
    }
}
