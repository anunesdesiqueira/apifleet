<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreAddress;
use App\Http\Requests\Enterprise\UpdateAddress;
use App\Services\Enterprise\AddressService;

class AddressController extends Controller
{
    /**
     * @var AddressService
     */
    protected $service;

    /**
     * AddressController constructor.
     * @param AddressService $service
     */
    public function __construct(AddressService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  StoreAddress  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreAddress $request)
    {
        $data = $request->only([
            'enterprise_id',
            'type',
            'address',
            'data_address'
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  UpdateAddress  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateAddress $request, $id)
    {
        $data = $request->only([
            'id',
            'type',
            'address',
            'data_address'
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
