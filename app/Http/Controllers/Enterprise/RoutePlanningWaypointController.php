<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreRoutePlanningWaypoint;
use App\Http\Requests\Enterprise\UpdateRoutePlanningWaypoint;
use App\Services\Enterprise\RoutePlanningWaypointService;

class RoutePlanningWaypointController extends Controller
{
    /**
     * @var RoutePlanningWaypointService
     */
    protected $service;

    /**
     * RoutePlanningWaypointController constructor.
     * @param RoutePlanningWaypointService $service
     */
    public function __construct(RoutePlanningWaypointService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_route_planning_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_route_planning_id)
    {
        return $this->service->all($enterprise_route_planning_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_route_planning_id
     * @param  StoreRoutePlanningWaypoint  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_route_planning_id, StoreRoutePlanningWaypoint $request)
    {
        $data = $request->only([
            'enterprise_route_planning_id',
	        'status',
	        'latitude',
	        'longitude',
        ]);
        
        return $this->service->store($enterprise_route_planning_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_route_planning_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_route_planning_id, $id)
    {
        return $this->service->show($enterprise_route_planning_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_route_planning_id
     * @param  UpdateRoutePlanningWaypoint  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_route_planning_id, UpdateRoutePlanningWaypoint $request, $id)
    {
        $data = $request->only([
            'id',
            'status',
	        'latitude',
	        'longitude',
        ]);

        return $this->service->update($enterprise_route_planning_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $enterprise_route_planning_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_route_planning_id, $id)
    {
        return $this->service->destroy($enterprise_route_planning_id, $id);
    }
}
