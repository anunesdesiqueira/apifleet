<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreRoutePlanning;
use App\Http\Requests\Enterprise\UpdateRoutePlanning;
use App\Services\Enterprise\RoutePlanningService;

class RoutePlanningController extends Controller
{
    /**
     * @var RoutePlanningService
     */
    protected $service;

    /**
     * RoutePlanningController constructor.
     * @param RoutePlanningService $service
     */
    public function __construct(RoutePlanningService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  StoreRoutePlanning  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreRoutePlanning $request)
    {
        $data = $request->only([
            'enterprise_id',
            'enterprise_driver_id',
	        'enterprise_vehicle_id',
	        'status',
	        'date',
	        'json',
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  UpdateRoutePlanning  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateRoutePlanning $request, $id)
    {
        $data = $request->only([
            'id',
            'enterprise_driver_id',
	        'enterprise_vehicle_id',
	        'status',
	        'date',
	        'json',
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
