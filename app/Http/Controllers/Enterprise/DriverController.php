<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Enterprise\StoreDriver;
use App\Http\Requests\Enterprise\UpdateDriver;
use App\Services\Enterprise\DriverService;

class DriverController extends Controller
{
    /**
     * @var DriverService
     */
    protected $service;

    /**
     * DriverController constructor.
     * @param DriverService $service
     */
    public function __construct(DriverService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $enterprise_id
     * @return \Illuminate\Http\Response
     */
    public function index($enterprise_id)
    {
        return $this->service->all($enterprise_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  StoreDriver  $request
     * @return \Illuminate\Http\Response
     */
    public function store($enterprise_id, StoreDriver $request)
    {
        $data = $request->only([
            'enterprise_id',
            'first_name',
	        'last_name',
	        'birth',
	        'license_number',
	        'license_type',
	        'license_expire_date',
	        'license_pic',
        ]);
        
        return $this->service->store($enterprise_id, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $enterprise_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enterprise_id, $id)
    {
        return $this->service->show($enterprise_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $enterprise_id
     * @param  UpdateDriver  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($enterprise_id, UpdateDriver $request, $id)
    {
        $data = $request->only([
            'id',
            'first_name',
	        'last_name',
	        'birth',
	        'license_number',
	        'license_type',
	        'license_expire_date',
	        'license_pic',
        ]);

        return $this->service->update($enterprise_id, $data, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enterprise_id, $id)
    {
        return $this->service->destroy($enterprise_id, $id);
    }
}
