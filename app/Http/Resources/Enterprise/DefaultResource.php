<?php

namespace App\Http\Resources\Enterprise;

use Illuminate\Http\Resources\Json\Resource;

class DefaultResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'enterprise_name' => $this->enterprise_name,
            'tax_id' => $this->tax_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
