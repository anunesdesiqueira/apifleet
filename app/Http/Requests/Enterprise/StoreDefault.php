<?php

namespace App\Http\Requests\Enterprise;

use App\Http\Requests\FormRequest;

class StoreDefault extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_name' => 'required',
            'tax_id' => 'nullable|integer',
            'user.default_language' => 'required',
            'user.email' => 'required|email|unique:users,email',
            'user.password' => 'required|confirmed',
            'user.first_name' => 'required',
            'user.last_name' => 'required',
            'user.mobile_phone' => 'required|unique:users,mobile_phone',
            'user.origin' => 'required|integer',
            'user.status_id' => 'required|exists:user_statuses,id',
            'user.receive_alert_by_email' => 'nullable|boolean',
            'user.unit_of_measure' => '',
            'user.picture' => '',
        ];
    }
}
