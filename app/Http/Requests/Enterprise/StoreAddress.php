<?php

namespace App\Http\Requests\Enterprise;

use App\Http\Requests\FormRequest;

class StoreAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_id' => 'required|exists:enterprises,id',
            'type' => 'required|integer',
            'address' => 'required',
            'data_address' => 'required',
        ];
    }
}
