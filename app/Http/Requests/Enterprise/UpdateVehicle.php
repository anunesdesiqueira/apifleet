<?php

namespace App\Http\Requests\Enterprise;

use App\Http\Requests\FormRequest;

class UpdateVehicle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:enterprise_vehicles,id',
            'number_plate' => 'required',
            'color' => 'required',
            'type' => 'required|integer',
            'load' => 'required',
        ];
    }
}
