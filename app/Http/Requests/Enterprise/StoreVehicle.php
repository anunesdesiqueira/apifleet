<?php

namespace App\Http\Requests\Enterprise;

use App\Http\Requests\FormRequest;

class StoreVehicle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_id' => 'required|exists:enterprises,id',
            'number_plate' => 'required',
            'color' => 'required',
            'type' => 'required|integer',
            'load' => 'required',
        ];
    }
}
