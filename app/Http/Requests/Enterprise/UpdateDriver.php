<?php

namespace App\Http\Requests\Enterprise;

use App\Http\Requests\FormRequest;

class UpdateDriver extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:enterprise_drivers,id',
            'first_name' => 'required',
            'last_name' => 'nullalble',
            'birth' => 'nullalble',
            'license_number' => 'nullalble',
            'license_type' => 'nullalble',
            'license_expire_date' => 'nullalble',
            'license_pic' => 'nullalble',
        ];
    }
}
