<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UpdateDefault extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:users,id',
            'default_language' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_phone' => 'required|unique:users,mobile_phone',
            'status_id' => 'required|exists:user_statuses,id',
            'receive_alert_by_email' => 'nullable|boolean',
            'unit_of_measure' => '',
            'picture' => '',
        ];
    }
}
