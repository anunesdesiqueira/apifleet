<?php

namespace App\Services\Enterprise;

use App\Repositories\Enterprise\DefaultRepository as EnterpriseRepository;
use App\Http\Resources\Enterprise\DefaultResource as EnterpriseResource;
use App\Traits\SendMailTrait;

class DefaultService
{
    use SendMailTrait;

    /**
     * @var EnterpriseRepository
     */
    protected $repository;

    /**
     * DefaultService constructor.
     * @param EnterpriseRepository $repository
     */
    public function __construct(EnterpriseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Response
     */
    public function all()
    {
        return EnterpriseResource::collection($this->repository->all())->response(200);
    }

    /**
     * @param array $data
     * @return Response
     */
    public function store(array $data)
    {
        try {
            $item = $this->repository->store($data);
            $item = $this->repository->enterpriseGetLastUserInsert($item->id);

            $user = $item->users->first();
            $status = $user->status;
            $activation = $user->activation;
            
            $this->queueMail($user->id, $user->email, $user->full_name, ['USER_NAME' => $user->first_name, 'ENTERPRISE_NAME' => $item->enterprise_name], 'get_start', $user->default_language);

            if ("USER_CREATED" === $status->status_name)
                $this->queueMail($user->id, $user->email, $user->full_name, ['USER_NAME' => $user->first_name, 'TOKEN' => $activation->token], 'confirm_email', $user->default_language);

        } catch(\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 400);
        }

        return (new EnterpriseResource($item))->response(201);
    }
    
    /**
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $item = $this->repository->show($id);

        } catch(\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 400);
        } 

        return (new EnterpriseResource($item))->response(200);
    }

    /**
     * @param array $data
     * @param int $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {
            $item = $this->repository->update($data, $id);

        } catch(\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 400);
        }

        return (new EnterpriseResource($item))->response(200);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
       return $this->repository->destroy($id)->response(200);
    }
}
