<?php

namespace App\Services\Enterprise;

use App\Repositories\Enterprise\AddressRepository;
use App\Http\Resources\Enterprise\AddressResource;

class AddressService
{
    /**
     * @var AddressRepository
     */
    protected $repository;

    /**
     * AddressService constructor.
     * @param AddressRepository $repository
     */
    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  int  $enterprise_id
     * @return Response
     */
    public function all($enterprise_id)
    {
        return AddressResource::collection($this->repository->all($enterprise_id))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param array $data
     * @return Response
     */
    public function store($enterprise_id, array $data)
    {
        try {
            $item = $this->repository->store($enterprise_id, $data);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new AddressResource($item))->response(201);
    }
    
    /**
     * @param  int  $enterprise_id
     * @param $id
     * @return Response
     */
    public function show($enterprise_id, $id)
    {
        try {
            $item = $this->repository->show($enterprise_id, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } 

        return (new AddressResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param array $data
     * @param int $id
     * @return Response
     */
    public function update($enterprise_id, array $data, $id)
    {
        try {
            $item = $this->repository->update($enterprise_id, $data, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new AddressResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param int $id
     * @return Response
     */
    public function destroy($enterprise_id, $id)
    {
       return $this->repository->destroy($enterprise_id, $id)->response(200);
    }
}
