<?php

namespace App\Services\Enterprise;

use App\Repositories\Enterprise\RoutePlanningWaypointRepository;
use App\Http\Resources\Enterprise\RoutePlanningWaypointResource;

class RoutePlanningWaypointService
{
    /**
     * @var RoutePlanningWaypointRepository
     */
    protected $repository;

    /**
     * RoutePlanningWaypointService constructor.
     * @param RoutePlanningWaypointRepository $repository
     */
    public function __construct(RoutePlanningWaypointRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  int  $enterprise_route_planning_id
     * @return Response
     */
    public function all($enterprise_route_planning_id)
    {
        return RoutePlanningWaypointResource::collection($this->repository->all($enterprise_route_planning_id))->response(200);
    }

    /**
     * @param  int  $enterprise_route_planning_id
     * @param array $data
     * @return Response
     */
    public function store($enterprise_route_planning_id, array $data)
    {
        try {
            $item = $this->repository->store($enterprise_route_planning_id, $data);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new RoutePlanningWaypointResource($item))->response(201);
    }
    
    /**
     * @param  int  $enterprise_route_planning_id
     * @param $id
     * @return Response
     */
    public function show($enterprise_route_planning_id, $id)
    {
        try {
            $item = $this->repository->show($enterprise_route_planning_id, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } 

        return (new RoutePlanningWaypointResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_route_planning_id
     * @param array $data
     * @param int $id
     * @return Response
     */
    public function update($enterprise_route_planning_id, array $data, $id)
    {
        try {
            $item = $this->repository->update($enterprise_route_planning_id, $data, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new RoutePlanningWaypointResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_route_planning_id
     * @param int $id
     * @return Response
     */
    public function destroy($enterprise_route_planning_id, $id)
    {
       return $this->repository->destroy($enterprise_route_planning_id, $id)->response(200);
    }
}
