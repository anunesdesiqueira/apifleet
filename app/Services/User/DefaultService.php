<?php

namespace App\Services\User;

use App\Repositories\User\DefaultRepository as UserRepository;
use App\Http\Resources\User\DefaultResource as UserResource;

class DefaultService
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * DefaultService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  int  $enterprise_id
     * @return Response
     */
    public function all($enterprise_id)
    {
        return UserResource::collection($this->repository->all($enterprise_id))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param array $data
     * @return Response
     */
    public function store($enterprise_id, array $data)
    {
        try {
            $item = $this->repository->store($enterprise_id, $data);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new UserResource($item))->response(201);
    }
    
    /**
     * @param  int  $enterprise_id
     * @param $id
     * @return Response
     */
    public function show($enterprise_id, $id)
    {
        try {
            $item = $this->repository->show($enterprise_id, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } 

        return (new UserResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param array $data
     * @param int $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {
            $item = $this->repository->update($enterprise_id, $data, $id);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return (new UserResource($item))->response(200);
    }

    /**
     * @param  int  $enterprise_id
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
       return $this->repository->destroy($enterprise_id, $id)->response(200);
    }
}
