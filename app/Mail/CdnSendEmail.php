<?php

namespace App\Mail;

use App\Repositories\Logger\LoggerMessageRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;

class CdnSendEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var string $baseUri
     */
    protected $baseUri;

    /**
     * int $userId
     */
    protected $userId;

    /**
     * @var string $template
     */
    protected $email;

    /**
     * @var string $template
     */
    protected $name;

    /**
     * @var array $params
     */
    protected $params;

    /**
     * @var string $template
     */
    protected $template;

    /**
     * @var string language
     */
    protected $language;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userId, $email, $name, $params, $template, $language = "en")
    {
        $this->baseUri = env('EMAIL_CDN_BASE_URI', 'https://cdn.t-rex.io');
        $this->userId = $userId;
        $this->email = $email;
        $this->name = $name;
        $this->params = $params;
        $this->template = $template;
        $this->language = $language;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(LoggerMessageRepository $repository)
    {
        $template = $this->buildTemplate();

        $repository->store([
            'user_id' => $this->userId, 'type' => $this->template, 'service' => 'SES', 'identifier' => $this->name, 'recipient' => $this->email, 'content' => $template['content']
        ]);

        return $this->to($this->email, $this->name)
            ->subject($template['subject'])
            ->view('mails.cdn', ['content' => $template['content']]);
    }

    /**
     * Build template
     */
    protected function buildTemplate()
    {
        $content = $this->getContentCdnEmail();
        if (!$content) {
            $this->language = 'en';
            $content = $this->getContentCdnEmail();
        }

        foreach ($this->params as $key => $value) {
            $content = str_replace(['[['. $key .']]'], [$value], $content);
        }

        preg_match('/<title>(.*?)<\/title>/', $content, $matches);
        $subject = $matches[1];

        return ['subject' => $subject, 'content' => $content];
    }

    /**
     * Get content
     */
    protected function getContentCdnEmail()
    {
        try {
            $response = (new Client(['base_uri' => $this->baseUri]))
                ->request('GET', '/emails/'. $this->language .'/'. $this->template .'.html');

            return $response->getBody()->getContents();

        } catch (\Exception $e) {
            return false;
        }
    }
}
