<?php

namespace App\Traits;

use App\Mail\CdnSendEmail;

trait SendMailTrait
{
    /**
     * Send email using class CdnSendEmail
     *
     * @param int $userId
     * @param string $email
     * @param string $name
     * @param array $params
     * @param string $template
     * @param string $language
     * @return void
     */
    public function queueMail($userId, $email, $name, array $params, $template, $language = "en")
    {
        \Mail::send(
            (new CdnSendEmail($userId, $email, $name, $params, $template, $language))->onQueue('users')
        );
    }
}
