<?php

namespace App\Traits;

trait EnterpriseTrait
{
    /**
     * Scope a query to only include for enterprise_id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFindEnterprise($query, $enterprise_id)
    {
        return $query->where('enterprise_id', $enterprise_id);
    }

    /**
     * Get the enterprise that owns the user.
     */
    public function enterprise()
    {
        return $this->belongsTo('App\Entities\Enterprise');
    }
}
