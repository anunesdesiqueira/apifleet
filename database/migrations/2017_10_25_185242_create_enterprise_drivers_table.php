<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->string('first_name', 30);
            $table->string('last_name', 30)->nullable();
            $table->date('birth')->nullable();
            $table->string('license_number', 50)->nullable()->unique();
            $table->string('license_type', 20)->nullable();
            $table->date('license_expire_date')->nullable();
            $table->text('license_pic')->nullable();
            $table->timestamps();

            $table->foreign('enterprise_id')->references('id')->on('enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_drivers');
    }
}
