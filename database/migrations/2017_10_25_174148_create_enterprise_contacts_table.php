<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->integer('role')->unsigned();
            $table->string('first_name', 30);
            $table->string('last_name', 30)->nullable();
            $table->string('landline', 40)->nullable();
            $table->string('mobile_phone', 45)->nullable()->unique();
            $table->string('email', 80)->nullable()->unique();
            $table->timestamps();

            $table->foreign('enterprise_id')->references('id')->on('enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_contacts');
    }
}
