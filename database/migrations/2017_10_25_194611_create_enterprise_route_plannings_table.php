<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseRoutePlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_route_plannings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->integer('enterprise_driver_id')->unsigned();
            $table->integer('enterprise_vehicle_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->date('date');
            $table->json('json')->nullable();
            $table->timestamps();
        });

        Schema::table('enterprise_route_plannings', function($table) {
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('enterprise_vehicle_id')->references('id')->on('enterprise_vehicles');
            $table->foreign('enterprise_driver_id')->references('id')->on('enterprise_drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_route_plannings');
    }
}
