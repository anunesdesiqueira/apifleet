<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->string('default_language', 10)->default('en');
            $table->string('email', 80)->unique();
            $table->string('password', 140);
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->string('mobile_phone', 45)->nullable()->unique();
            $table->enum('origin', ['ios','android','web']);
            $table->integer('status_id')->unsigned();
            $table->string('unit_of_measure', 45)->default('Metric');
            $table->boolean('receive_alert_by_email')->default(1);
            $table->text('picture')->nullable();
            $table->timestamps();

            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('status_id')->references('id')->on('user_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
