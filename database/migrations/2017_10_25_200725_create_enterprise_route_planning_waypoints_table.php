<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseRoutePlanningWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_route_planning_waypoints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_route_planning_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->decimal('latitude', 9, 6);
            $table->decimal('longitude', 9, 6);
            $table->timestamps();

            $table->foreign('enterprise_route_planning_id', 'route_planning_id_foreign')->references('id')->on('enterprise_route_plannings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_route_planning_waypoints');
    }
}
