<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_statuses')->insert([
        	[
	        	'id' => 1,
	            'status_name' => 'USER_CREATED',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 2,
	            'status_name' => 'USER_ACTIVATED',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 3,
	            'status_name' => 'USER_PASSWORD_BLOCKED',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ]
        ]);
    }
}
