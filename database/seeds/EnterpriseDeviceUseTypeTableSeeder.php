<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EnterpriseDeviceUseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enterprise_device_use_types')->insert([
        	[
	        	'id' => 1,
	            'device_use_type' => 'CAR',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 2,
	            'device_use_type' => 'PET',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 3,
	            'device_use_type' => 'MOTORBIKE',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 4,
	            'device_use_type' => 'SENIOR',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 5,
	            'device_use_type' => 'PACKAGING',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 6,
	            'device_use_type' => 'BICYCLE',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ],
	        [
	        	'id' => 7,
	            'device_use_type' => 'GENERIC',
	            'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
	        ]
        ]);
    }
}
